<?php

namespace Inmovsoftware\CommunityApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Community extends Model
{
    use SoftDeletes;
    protected $table = "it_posts";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['it_business_id', 'photo', 'text', 'date', 'it_users_id', 'status'];



    public function User()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'it_users_id', 'id');

    }


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".text", 'like', "%$param%");
    }

    public static function scopethisUser(){
        $$query->it_users_id = auth()->user()->id;
    }

}
