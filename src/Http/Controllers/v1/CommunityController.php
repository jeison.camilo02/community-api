<?php
namespace Inmovsoftware\CommunityApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\CommunityApi\Models\V1\Community;
use Inmovsoftware\CommunityApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CommunityController extends Controller
{
    public function index(Request $request)
    {
        $filter = "text";
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sortField = "date";

        $item = Community::orderBy($sortField, $sortOrder);
        if (!empty($filterValue)) {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }
        $item->with('User');
        $item->where('status', 'A');
        return new GlobalCollection($item->paginate($pageSize));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C"
        ]);
        $InsertId = Community::insertGetId($data);
        $inserted = Community::where("id", $InsertId)->get();
        return response()->json($inserted);
    }


    /**
     * Display the specified resource.
     *
     * @param  Inmovsoftware\CommunityApi\Models\V1\Community $community
     * @return \Illuminate\Http\Response
     */
    public function show(Community $community)
    {

        return response()->json($community);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Inmovsoftware\CommunityApi\Models\V1\Community $community
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Community $community)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C"
        ]);

        $community->update($data);
        return response()->json($community);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inmovsoftware\CommunityApi\Models\V1\Community $community
     * @return \Illuminate\Http\Response
     */
    public function destroy(Community $community)
    {
        $item = $community->delete();
        if ($item) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.element_not_found')]
                    ]
                ],
                401
            );
                    } else {
            return response()->json($item);
                    }
    }

}
